
//-------------------------START----------------------//
 //start
const startButton = document.querySelector('#hiddenStart');
const start = startButton.addEventListener('click',startGame);
//game over
const gameOver = document.querySelector('#hiddenGameOver');
const retryButton = document.querySelector('#retryButton');

function startGame(){

    const playground = document.querySelector('section');
    const target = document.querySelector('#target');

    //----------------------------Style-------------------------//
    //background
    playground.style.backgroundImage = "url('images/space.jpg')";
    playground.style.backgroundRepeat = "no-repeat";
    playground.style.backgroundSize = "cover";
    playground.style.cursor ="url('images/cursor.png'), auto";
    //start
    startButton.style.display = "none";
    const titleStart = document.querySelector('h2');
    titleStart.style.display = "none";
    //Target
    target.style.display = "initial";
    //Game Over
    gameOver.style.display = "none";
    //Mouse
    //code
    //----------------------------------------------------------//

    //------------------Create Target + Timer ------------------//
    const myTimeout = setInterval(createTarget, 1000);

    function createTarget() {
        const randomX = Math.random() * 740;
        const randomY = Math.random() * 490;
                
        target.style.top = randomY + "px";
        target.style.left = randomX + "px";
    }
     //----------------------------------------------------------//
        
    //----------------------- Add player -----------------------//
    target.addEventListener('click', targetClicked);
    let score = 0;

    function targetClicked(){
        score++;

    }
    //-----------------------------------------------------------//

    // ---------------------- CountDown -------------------------//
    const divTimer = document.querySelector("#showTimer"); //ID 
    const timer = setInterval(countDownTimer, 1000);

    let timeLeft = 30; // 30 seconds
    //let timeLeft = 10; // debug
    function countDownTimer(){
        timeLeft--;
                
        if(timeLeft === -1){
            clearInterval(timer);
            gameOverShow();

        } else{
            divTimer.innerText = "Timer: "+ timeLeft + ' seconds';
        }
    }
    //----------------------------------------------------------//

    //-----------------------GAME OVER------------------------//
    function gameOverShow(){
        //Style
        playground.style.backgroundImage = "none";
        playground.style.backgroundColor = "rgb(70, 70, 250)";
        target.style.display = "none";
        gameOver.style.display = "initial";

        //Show Your Score + Style
        const finalScore = document.querySelector('#allScore').innerHTML = score;

        if(finalScore >= 0 && finalScore <= 10){
            document.querySelector('#text-score').innerText = "Defeat is just a stepping stone, keep striving for victory!"; 
        } else if(finalScore > 10 && finalScore <= 20){
            document.querySelector('#text-score').innerText = "Though you didn't win, your effort was commendable. The adventure continues."; 
        } else {
             document.querySelector('#text-score').innerText = "Victory is yours! You've conquered the challenge."; 
        }

        //RETRY BUTTON
        retryButton.addEventListener('click',startGame);
        clearInterval(myTimeout);
    }
    //----------------------------------------------------------//
}
//---------------------------END-------------------------------//
